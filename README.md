# 10up Front End Engineering Technical Challenge
[![Netlify Status](https://api.netlify.com/api/v1/badges/7979a69b-8e6a-498d-a300-d9ab7d00c9aa/deploy-status)](https://app.netlify.com/sites/10up-frontend/deploys)

You can see live demo [here](https://10up-frontend.netlify.app/). 

before you start make sure you have node installed 
* Node version: 14.15
* Yarn

## Available script
* `yarn start` to start the development server.
* `yarn build` to build the assets for a production build.

## Project files
> `src` folder contains the development files.

> `assets` folder is where you can find your production build.

> `gulpfile.js` contains the build configuration tasks. 
