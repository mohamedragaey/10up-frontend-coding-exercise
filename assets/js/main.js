try {
  /* eslint-disable */
  const element = document.getElementById('headerMenuNav')
  const menuButton = document.getElementById('menuBtn')
  const menuCloseBtn = document.getElementById('menuCloseBtn')
  
  menuButton.onclick = function () {
    element.classList.toggle('active')
    menuButton.classList.toggle('active')
    menuCloseBtn.classList.toggle('active')
  }
  
  menuCloseBtn.onclick = function () {
    element.classList.toggle('active')
    menuButton.classList.toggle('active')
    menuCloseBtn.classList.toggle('active')
  }
  
  /* eslint-enable */
} catch (e) {
  console.log('Error Message:' + e.message)
}
