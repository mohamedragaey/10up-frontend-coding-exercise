const gulp = require('gulp'),
  sass = require('gulp-sass'),
  notify = require('gulp-notify'),
  uglify = require('gulp-uglify'),
  cssnano = require('gulp-cssnano'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  dirSync = require('gulp-directory-sync'),
  sourcemaps = require('gulp-sourcemaps'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  autoprefixer = require('gulp-autoprefixer'),
  standard = require('gulp-standard'),
  include = require('gulp-include'),
  environments = require('gulp-environments')
const development = environments.development
const production = environments.production
const config = {
    npmDir: '/node_modules',
    publicRootDir: './assets'
  },
  srcs = {
    scss: './src/scss',
    js: './src/js',
    img: './src/images'
  },
  dests = {
    css: config.publicRootDir + '/css',
    js: config.publicRootDir + '/js',
    img: config.publicRootDir + '/images'
  }

gulp.task('sync-images', function () {
  return gulp.src(srcs.img + '/**/*', {base: srcs.img})
    .pipe(production(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5,
      svgoPlugins: [{removeViewBox: true}],
      verbose: true,
      use: [pngquant()]
    })))
    .pipe(gulp.dest(dests.img))
    .pipe(dirSync(srcs.img, dests.img, {printSummary: false, ignore: '.gitignore'}))
    .on('error', notify.onError(function (error) {
      return 'Error: ' + error.message
    }))
})

gulp.task('css', function () {
  return gulp.src(srcs.scss + '/style.scss')
    .pipe(development(sourcemaps.init({loadMaps: true})))
    .pipe(sass({
      style: 'expanded',
      includePaths: [
        __dirname + srcs.scss,
        __dirname + config.npmDir
      ]
    }).on('error', notify.onError(function (error) {
      return 'Error: ' + error.message
    })))
    // auto prefix and keep last two browser versions
    .pipe(production(autoprefixer({})))
    .pipe(gulp.dest(dests.css))
    .pipe(rename({suffix: '.min'}))
    .pipe(production(cssnano({discardComments: {removeAll: true}})))
    .pipe(development(sourcemaps.write('.')))
    .pipe(gulp.dest(dests.css))
})

gulp.task('scripts', function () {
  return gulp.src(srcs.js + '/scripts.js')
    .pipe(include({
      extensions: 'js',
      includePaths: [
        __dirname + srcs.js,
        __dirname + config.npmDir
      ]
    }))
    .pipe(concat('main.js'))
    .pipe(gulp.dest(dests.js))
    .pipe(rename({suffix: '.min'}))
    .pipe(production(uglify().on('error', notify.onError(function (error) {
      return 'Error compiling JavaScript: ' + error.message
    }))))
    .pipe(gulp.dest(dests.js))
})

gulp.task('standard', function () {
  return gulp.src(srcs.js + '/**/*.js')
    .pipe(standard())
    .pipe(standard.reporter('default', {
      breakOnError: false,
      quiet: true
    }))
})

gulp.task('watch', gulp.parallel(function () {
  gulp.watch(srcs.scss + '/**/*.scss', gulp.series('css'))
  gulp.watch(srcs.js + '/**/*.js', gulp.series('scripts'))
  gulp.watch(srcs.js + '/**/*.js', gulp.series('standard'))
}))

gulp.task('dev', gulp.series(['css', 'scripts', 'sync-images', 'watch']))

gulp.task('prod', gulp.series(['css', 'scripts', 'sync-images', 'standard']))
